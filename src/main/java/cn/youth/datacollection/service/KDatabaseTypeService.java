package cn.youth.datacollection.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.youth.datacollection.entity.KDatabaseType;

public interface KDatabaseTypeService extends IService<KDatabaseType> {
}
