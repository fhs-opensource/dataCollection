package cn.youth.datacollection.service.impl;

import cn.youth.datacollection.entity.KFailRetryLog;
import cn.youth.datacollection.mapper.KFailRetryLogMapper;
import cn.youth.datacollection.service.KFailRetryLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @Description
 * @auther chen1
 * @create 2022-04-05 17:32
 */
@Service
public class KFailRetryLogServiceImpl extends ServiceImpl<KFailRetryLogMapper, KFailRetryLog> implements KFailRetryLogService {
}
